//
// Created by nicholas.robbins on 10/21/2018.
//

#ifndef TRANSFORM_HPP
#define TRANSFORM_HPP

#include "Component.hpp"

class Transform: public Component
{
    float x, y, z;

public:
    Transform();
    ~Transform();

    virtual void update();
};



#endif //_TRANSFORM_HPP
