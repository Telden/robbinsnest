//
// Created by nicholas.robbins on 10/31/2018.
//

#ifndef UNTITLED_SPRITE_HPP
#define UNTITLED_SPRITE_HPP

#include "../Component.hpp"

struct Sprite
{
    int x, y;
    int spriteWidth;
    int spriteHeight;
    int r, g, b, a;


};



#endif //UNTITLED_SPRITE_HPP
