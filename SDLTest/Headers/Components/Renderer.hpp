//
// Created by nicholas.robbins on 10/31/2018.
//
#ifndef UNTITLED_RENDERER_HPP
#define UNTITLED_RENDERER_HPP

#include "../Component.hpp"
#include "SDL2/SDL.h"

struct Sprite;

class Renderer : public Component
{
    SDL_Renderer* mpRenderer;

public:

    void update() __override;
    void initialize();
    void drawSprite(Sprite* spriteOb);

};






#endif //UNTITLED_RENDERER_HPP
