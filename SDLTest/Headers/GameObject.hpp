//
// Created by nicholas.robbins on 10/20/2018.
//

#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

#include <memory>
#include <vector>


#include "Component.hpp"


class GameObject
{
    std::vector<std::unique_ptr<Component>> components;
    int instanceID;

public:
    GameObject();
    GameObject(int ID);
    ~GameObject();


    template<class T>
    void addComponent(T newComp)
    {
        //If not already attached
        if(!findComponent(newComp))
        {
            auto tmpComp = components.emplace_back( std::make_unique<T>()) . get();
        }
    }
    template<class T>
    void removeComponent(T targetComp)
    {
        //https://stackoverflow.com/questions/875103/how-do-i-erase-an-element-from-stdvector-by-index
        //check if the object exists
        if(findComponent(targetComp))
        {
           for(int i = 0; i < components.size(); i++)
           {
               if(dynamic_cast<T*>(components[i]) != nullptr)
               {
                   components.erase(components.begin() + i);
               }
           }
        }
    }
    template<class T>
    bool findComponent(T searchComp)
    {

        for (const auto &component : components)
        {
            if(dynamic_cast<T*>(component.get()) != nullptr)
                return true;
        }
        return false;
    }

    template<class T>
    T getComponent(T targetComp)
    {
        for (const auto &component : Components)
        {
            if(dynamic_cast<T*>(component.get()) != nullptr)
                return component.get();
        }

        return nullptr

    }
};





#endif //GAMEOBJECT_HPP
