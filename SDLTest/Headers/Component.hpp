//
// Created by nicholas.robbins on 10/20/2018.
//

#ifndef COMPONENT_HPP
#define COMPONENT_HPP


class Component
{

public:
    Component();
    ~Component();

    virtual void update();



};




#endif //UNTITLED_COMPONENT_HPP
