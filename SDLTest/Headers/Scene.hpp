//
// Created by nicholas.robbins on 10/20/2018.
//

#ifndef SCENE_HPP
#define SCENE_HPP



#include<unordered_map>
#include<string>
#include<memory>

class GameObject;

class Scene
{
    //Unordered map with an instance as the key with a unique pointer to the gameobject
    std::unordered_map<int, std::unique_ptr<GameObject>> sceneObjects;

    //Scene keeps track of the next object instance ID (for now)
    int nextInstance;

public:

    Scene();
    ~Scene();

    int createGameobject();
    void destroyGameObject(int targetID);
    bool findGameObject(int targetID);
    int getNextInstance();

    void updateGameobjects();


};




#endif //SCENE_HPP
