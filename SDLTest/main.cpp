#include <iostream>
#include "SDL2/SDL.h"

//int main(int argv, char ** argc) {
//
//    SDL_Init(SDL_INIT_VIDEO);
//    std::cout << "Hello, World!" << std::endl;
//
//    std::cin.get();
//
//    SDL_Quit();
//    return 0;
//}

SDL_Window *MainWindow;
SDL_GLContext Context;

int main(int argv, char ** argc )
{
    SDL_Init(SDL_INIT_VIDEO);
    MainWindow = SDL_CreateWindow("Setup Window", 0, 0, 640, 480, SDL_WINDOW_OPENGL);
    Context = SDL_GL_CreateContext(MainWindow);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);


    std::cin.get();

    SDL_GL_DeleteContext(Context);
    SDL_DestroyWindow(MainWindow);
    SDL_Quit();


    return 0;

}