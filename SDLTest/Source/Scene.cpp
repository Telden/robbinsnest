//
// Created by nicholas.robbins on 10/21/2018.
//

#include "../Headers/Scene.hpp"
#include "../Headers/GameObject.hpp"

Scene::Scene()
{
    nextInstance = 0;
}

Scene::~Scene()
{
    while(sceneObjects.size() > 0)
    {
        sceneObjects.erase(sceneObjects.begin());
    }
}

int Scene::createGameobject()
{
    int ID = getNextInstance();
    sceneObjects.insert(std::make_pair(ID, new GameObject(ID)));
    return ID;
}

void Scene::destroyGameObject(int ID)
{
    sceneObjects.erase(ID);
}

bool Scene::findGameObject(int ID)
{
    std::unordered_map<int, std::unique_ptr<GameObject>>::const_iterator iter = sceneObjects.find(ID);
    if(iter == sceneObjects.end())
        return false;
    else
        return true;
}

void Scene::updateGameobjects()
{
    for(const auto & [ key, value] :  sceneObjects)
    {

    }
}


int Scene::getNextInstance()
{
    nextInstance++;
    return nextInstance;
}
