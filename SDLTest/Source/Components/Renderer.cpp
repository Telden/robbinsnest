//
// Created by nicholas.robbins on 10/31/2018.
//
#include "../../Headers/Components/Sprite.hpp"
#include "../../Headers/Components/Renderer.hpp"

void Renderer::update()
{

}

void Renderer::initialize()
{


}

void Renderer::drawSprite(Sprite *spriteOb)
{
    SDL_SetRenderDrawColor(mpRenderer, spriteOb->r, spriteOb->g, spriteOb->b, spriteOb->a);

    SDL_RenderClear(mpRenderer);

    SDL_Rect tmp;

    tmp.x = spriteOb->x;
    tmp.y = spriteOb->y;
    tmp.w = spriteOb->spriteWidth;
    tmp.h = spriteOb->spriteHeight;


    SDL_SetRenderDrawColor(mpRenderer, spriteOb->r, spriteOb->g, spriteOb->b, spriteOb->a)
    SDL_RenderFillRect(mpRenderer, &tmp);
}