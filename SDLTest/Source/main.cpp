#include <iostream>
#include "../Headers/Scene.hpp"
#include "../Headers/Transform.hpp"
#include "../Headers/GameObject.hpp"
#include<memory>
int main() {
    std::cout << "Hello, World!" << std::endl;
    Scene newScene;
    int gameObjectID = newScene.createGameobject();
    newScene.destroyGameObject(gameObjectID);

    GameObject newGO(gameObjectID);
    Transform testTransform();

    //newGO.addComponent(testTransform);


    return 0;
}